package ch.fhnw.mada.rsa.generateKey;

import java.math.BigInteger;
import java.util.Random;

/**
 * Created by Steven on 17.03.2016.
 */
public abstract class Generator {
	static BigInteger one = new BigInteger("1");
	static BigInteger zero = new BigInteger("0");

	/**
	 * Generates key elements
	 * @return [n, d, e]
     */
	static public BigInteger[] generateKey() {
		BigInteger p, q, n, d, e, m;
		p = createPrimNumber(1024);
		do {
			q = createPrimNumber(1024);
		} while (p.compareTo(q) == 0);
		n = p.multiply(q);
		m = (p.subtract(one)).multiply(q.subtract(one));
		do {
			do {
				e = createPrimNumber(2048);
			} while (e.compareTo(m) == -1 && gcd(e, n)[0].compareTo(one) == 0);
			d = gcd(m, e)[1];
		} while (d.compareTo(zero) != 1);
		return new BigInteger[] {n, d, e};
	}

	/**
	 * Creates random bits prim number
	 * @param bits number of bits
	 * @return random prim number
     */
	static private BigInteger createPrimNumber(int bits) {
		return new BigInteger(bits, 100, new Random());
	}

	/**
	 * Greatest common divisor
	 * @param m phi(n)
	 * @param e selected part of public key
     * @return [gcd, d]
     */
	static private BigInteger[] gcd(BigInteger m, BigInteger e)
	{
		BigInteger a = m;
		BigInteger b = e;
		BigInteger x0 = new BigInteger("1");
		BigInteger y0 = new BigInteger("0");
		BigInteger x1 = new BigInteger("0");
		BigInteger y1 = new BigInteger("1");
		BigInteger q;
		BigInteger r;
		BigInteger u0;
		BigInteger u1;
		do {
			q = a.divide(b);
			r = a.mod(b);
			u0 = x0;
			u1 = y0;
			a = b;
			b = r;
			x0 = x1;
			y0 = y1;
			x1 = u0.subtract(q.multiply(x1));
			y1 = u1.subtract(q.multiply(y1));
		} while ( b.compareTo(zero) != 0);
		return new BigInteger[] {a, y0};
	}
}
