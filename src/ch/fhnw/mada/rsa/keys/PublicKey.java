package ch.fhnw.mada.rsa.keys;

import java.math.BigInteger;

/**
 * Created by Claudio on 10.03.2016.
 */
public class PublicKey extends Key {
    protected BigInteger e;

    public PublicKey(BigInteger n, BigInteger e){
        super(n);
        this.e = e;
    }

    public BigInteger getN() {
        return this.n;
    }

    public BigInteger getE(){
        return this.e;
    }
}
