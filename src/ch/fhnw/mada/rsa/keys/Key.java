package ch.fhnw.mada.rsa.keys;

import java.math.BigInteger;

/**
 * Created by Claudio on 10.03.2016.
 */
public abstract class Key {
    protected BigInteger n;

    public Key(BigInteger n){
        this.n = n;
    }
}
