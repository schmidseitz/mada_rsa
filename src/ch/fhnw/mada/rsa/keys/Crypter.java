package ch.fhnw.mada.rsa.keys;

import java.math.BigInteger;

/**
 * Created by Claudio on 10.03.2016.
 */
public class Crypter {
    private PrivateKey privateKey;
    private PublicKey publicKey;
    private PublicKey externalPublicKey;

    public Crypter(){
        //
    }

    public void setPrivateKey(PrivateKey privateKey) {
        this.privateKey = privateKey;
    }
    public PrivateKey getPrivateKey() {return this.privateKey; }
    public void setPublicKey(PublicKey publicKey) {
        this.publicKey = publicKey;
    }
    public PublicKey getPublicKey(){
        return this.publicKey;
    }
    public void setExternalPublicKey(PublicKey externPublicKey) {
        this.externalPublicKey = externPublicKey;
    }
    public PublicKey getExternalPublicKey() {
        return this.externalPublicKey;
    }

    /**
     * Checks state of crypter
     * @return state of  crypter components
     */
    public boolean isReadyToEncrypt(){
        return privateKey != null;
    }
    public boolean isReadyToCrypt(){
        return externalPublicKey != null;
    }
    public boolean isReadyToExport(){
        return publicKey != null;
    }

    /**
     * Encrypts a number with the external public key
     * @param c number to encrypt
     * @return encrypted number
     */
    public BigInteger encrypt(int c) {
        return iteratedSquare(BigInteger.valueOf(c), externalPublicKey.getE(), externalPublicKey.getN());
    }

    /**
     * Decrypts a character with the private key
     * @param cChar character to decrypt
     * @return decrypted character
     */
    public char decrypt(String cChar){
        BigInteger base = new BigInteger(cChar);
        return (char)iteratedSquare(base, privateKey.getD(), privateKey.getN()).intValue();
    }

    /**
     * Decrypts / Encrypts with the iterated square method RSA
     * @param base number to decrypt / encrypt
     * @param e part d of the private key / part e of the external public key
     * @param m part n of private / external public key
     * @return decrypted / encrypted number
     */
    private  BigInteger iteratedSquare (BigInteger base,BigInteger e, BigInteger m){
        String bits = e.toString(2);
        int i = bits.length();
        BigInteger h = BigInteger.valueOf(1L);
        BigInteger k = base;
        while(i > 0){
            if (bits.charAt(i-1) == '1') {
                h = (h.multiply(k)).mod(m);
            }
            k = (k.multiply(k)).mod(m);
            i -= 1;
        }
        return h;
    }
}
