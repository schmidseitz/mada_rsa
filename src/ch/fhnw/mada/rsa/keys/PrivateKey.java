package ch.fhnw.mada.rsa.keys;

import java.math.BigInteger;

/**
 * Created by Claudio on 10.03.2016.
 */
public class PrivateKey extends Key{
    private BigInteger d;

    public PrivateKey(BigInteger n, BigInteger d) {
        super(n);
        this.d = d;
    }

    public BigInteger getN() {
        return this.n;
    }

    public BigInteger getD() {
        return this.d;
    }
}
