package ch.fhnw.mada.rsa.gui;
import ch.fhnw.mada.rsa.generateKey.Generator;
import ch.fhnw.mada.rsa.keys.*;

import javax.swing.*;
import java.io.*;
import java.math.BigInteger;
import java.util.*;
import java.util.concurrent.CancellationException;
import java.util.regex.Pattern;

/**
 * Created by Claudio on 10.03.2016.
 */
public abstract class Master {
    /**
     * Masterclass
     */
    static private Crypter crypter;
    static protected String aDir;
    static CrypterGUI frame;

    /**
     * Main, initialize
     * @param args
     */
    static public void main(String[] args) {
        try {
            //initialize GUI
            aDir = System.getProperty("user.dir");
            frame = new CrypterGUI();
            frame.setStartFolder(aDir);
            crypter = new Crypter();
            frame.printKeys("null", "null", "null");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    /**
     * Encrypts a file with the external public key
     * Prefix of the file: e
     * @param pathFile file to encrypt
     * @param output output folder
     * @return path of the output file
     */
    static String encryptFile(String pathFile, String output) {
        if(!crypter.isReadyToCrypt()){
            throw new NullPointerException("Current external public key is invalid");
        }
        File file = new File(pathFile);
        File cryptFile = new File(output + "\\e" + file.getName());
        if (cryptFile.exists()) {
            if (JOptionPane.showConfirmDialog(null, "File: " + cryptFile.getPath() + " already exists. Do you want to overwrite?", "MadaRSA", JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION){
                throw new CancellationException("Cancelled encrypting file: " + file.getPath());
            }
        }
        List<String> list = new ArrayList<>();
        try (BufferedReader rd = new BufferedReader(new FileReader(file.getPath()))){
            rd.lines().forEach(x -> {
                x.chars().forEach(y -> list.add(crypter.encrypt(y).toString(10)));
                list.add(crypter.encrypt('\n').toString(10));
            });
        }
        catch (Exception ex) {
            frame.textlnTAFile(ex.getMessage());
        }
        try (BufferedWriter wr = new BufferedWriter(new FileWriter(cryptFile.getPath()))){
            wr.write(String.join("," , list));
        }
        catch (Exception ex) {
            frame.textlnTAFile(ex.getMessage());
        }
        return cryptFile.getPath();
    }

    /**
     * Decrypts a file with the private key
     * Prefix of the file: d
     * @param pathFile file to decrypt
     * @param output output folder
     * @return path of the output file
     */
    static String decryptFile(String pathFile, String output) {
        if(!crypter.isReadyToEncrypt()){
            throw new NullPointerException("Current private key is invalid");
        }
        File file = new File(pathFile);
        File encryptFile = new File(output + "\\d" + file.getName());
        if (encryptFile.exists()) {
            if (JOptionPane.showConfirmDialog(null, "File: " + encryptFile.getPath() + " already exists. Do you want to overwrite?", "MadaRSA", JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION){
                throw new CancellationException("Cancelled decrypting file: " + file.getPath());
            }
        }
        List<String> list = new ArrayList<>();
        StringBuilder sb1 = new StringBuilder(), sb2 = new StringBuilder();
        try (BufferedReader rd = new BufferedReader(new FileReader(file.getPath()))) {
            rd.lines().forEach(x -> sb1.append(x));
        }
        catch (Exception ex) {
            frame.textlnTAFile(ex.getMessage());
        }
        if (!sb1.toString().matches("\\d+[,\\d]*")){
            throw new NullPointerException("Decrypting failed:\n" +
                    "Imported file: " + file.getPath() + " has an illegal format");
        }
        Collections.addAll(list, sb1.toString().split(","));
        list.forEach(x -> sb2.append(crypter.decrypt(x)));
        try (BufferedWriter wr = new BufferedWriter(new FileWriter(encryptFile.getPath()))){
            wr.write(sb2.toString());
        }
        catch (Exception ex) {
            frame.textlnTAFile(ex.getMessage());
        }
        return encryptFile.getPath();
    }

    /**
     * Exports the public key
     * Format of key: (<n>,<e>)
     * Output file: *\publicKey.txt
     * @param keyNum number of key
     * @param output output folder
     * @return name of key, path of the output file
     */
    static String[] exportKey(int keyNum, String output){
        if(!crypter.isReadyToExport()){
            throw new NullPointerException("Current public key is invalid");
        }
        File f = null;
        String keyName = "";
        switch (keyNum){
            case 0:
                //private key
                f = new File(output + "\\privateKey.txt");
                keyName = "private";
                break;
            case 1:
                //public key
                f = new File(output + "\\publicKey.txt");
                keyName = "public";
                break;
            case 2:
                //external public key
                f = new File(output + "\\exPublicKey.txt");
                keyName = "external public";
                break;
        }
        if (f.exists()) {
            if (JOptionPane.showConfirmDialog(null, "File: " + f.getPath() + " already exists. Do you want to overwrite?", "MadaRSA", JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION){
                throw new CancellationException("Cancelled exporting public key: " + f.getPath());
            }
        }
        StringBuilder sb = new StringBuilder();
        sb.append('(');
        switch (keyNum){
            case 0:
                //private key
                sb.append(crypter.getPrivateKey().getN().toString(10) + ',' +crypter.getPrivateKey().getD().toString(10));
                break;
            case 1:
                //public key
                sb.append(crypter.getPublicKey().getN().toString(10) + ',' +crypter.getPublicKey().getE().toString(10));
                break;
            case 2:
                //external public key
                sb.append(crypter.getExternalPublicKey().getN().toString(10) + ',' +crypter.getExternalPublicKey().getE().toString(10));
                break;
        }
        sb.append(')');
        try (BufferedWriter wr = new BufferedWriter(new FileWriter(f.getPath()))){
            wr.write(sb.toString());
        }
        catch (Exception ex) {
            frame.textlnTAFile(ex.getMessage());
        }
        return new String[] { keyName, f.getPath()};
    }

    /**
     * Import an external key
     * Format of key: (<n>,<e>)
     * Format of key: (<n>,<d>)
     * @param keyNum number of key
     * @param filePath key to import
     * @return name of key
     */
    static String importKey(int keyNum, String filePath){
        StringBuilder sb = new StringBuilder();
        try(BufferedReader rd = new BufferedReader(new FileReader(filePath))){
            rd.lines().forEach(x -> sb.append(x));
        }
        catch (Exception ex) {
            frame.textlnTAFile(ex.getMessage());
        }
        if(!sb.toString().matches(Pattern.quote("(") +"\\d+,\\d+" + Pattern.quote(")"))) {
            throw new NullPointerException("Imported file has an illegal key format");
        }
        String[] pKeys = frame.getPrintedKeys();
        String[] exKey = sb.substring(1, sb.length() - 1).split(",");
        switch (keyNum){
            case 0:
                //private key
                crypter.setPrivateKey(new PrivateKey(new BigInteger(exKey[0]), new BigInteger(exKey[1])));
                frame.printKeys(sb.toString(), pKeys[1], pKeys[2]);
                return "private";
            case 1:
                //public key
                crypter.setPublicKey(new PublicKey(new BigInteger(exKey[0]), new BigInteger(exKey[1])));
                frame.printKeys(pKeys[0], sb.toString(), pKeys[2]);
                return "public";
            case 2:
                //external public key
                crypter.setExternalPublicKey(new PublicKey(new BigInteger(exKey[0]), new BigInteger(exKey[1])));
                frame.printKeys(pKeys[0], pKeys[1], sb.toString());
                return "external public";
        }
        return null;
    }
    /**
     * generates private and public keys with RSA method
     */
    static void generateKey() {
        BigInteger[] bigs = Generator.generateKey();
        crypter.setPrivateKey(new PrivateKey(bigs[0], bigs[1]));
        crypter.setPublicKey(new PublicKey(bigs[0], bigs[2]));
        frame.printKeys("(" + bigs[0].toString(10) + "," + bigs[1].toString(10) + ")", "(" + bigs[0].toString(10) + "," + bigs[2].toString(10) + ")" , "null");
    }

    /**
     * Use public key as external public key
     */
    static void useAsExternalKey() {
        if(!crypter.isReadyToExport()){
            throw new NullPointerException("Current public key is invalid");
        }
        crypter.setExternalPublicKey(crypter.getPublicKey());
        String[] pKeys = frame.getPrintedKeys();
        frame.printKeys(pKeys[0], pKeys[1], "(" + crypter.getPublicKey().getN().toString(10) + "," + crypter.getPublicKey().getE().toString(10) + ")");
    }
}
