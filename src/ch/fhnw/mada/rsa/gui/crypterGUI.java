package ch.fhnw.mada.rsa.gui;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

/**
 * Created by Claudio on 17.03.2016.
 */
public class CrypterGUI extends JFrame{
    private JTextField JTFFilePath;
    private JButton JBEncrypt;
    private JButton JBDecrypt;
    private JTextField JTFOutput;
    private JButton JBGenKey;
    private JButton JBPubKeyAsEx;
    private JTextArea JTAFile;
    private JButton JBFilePathFD;
    private JButton JBOutputFD;
    private JComboBox JCBSelKey;
    private JLabel JLSelKey;
    private JButton JBImpKey;
    private JButton JBExportKey;
    private JPanel JMainPanel;
    private JPanel JPanKey;
    private JPanel JPanKeyIO;
    private JPanel JPanFileIO;
    private JPanel JPanGen;
    private JPanel JPanFile;
    private JTextArea JTAKey;
    private JScrollPane JScPanKey;
    private JLabel JLFilePath;
    private JLabel JLOutput;
    private JScrollPane JScPanFile;

    public CrypterGUI() {
        /**
         * Initialize GUI
         */
        super();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setTitle("RSA Crypter");
        setSize(600, 500);
        setContentPane(JMainPanel);
        JCBSelKey.addItem("Private key");
        JCBSelKey.addItem("Public key");
        JCBSelKey.addItem("External public key");
        JCBSelKey.setSelectedIndex(2);
        setVisible(true);
        /**
         * Select text file to encrypt / decrypt
         */
        JBFilePathFD.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser jfc = new JFileChooser();
                jfc.setCurrentDirectory(new File(Master.aDir));
                jfc.setMultiSelectionEnabled(false);
                jfc.setFileFilter(new FileFilter() {
                    @Override
                    public boolean accept(File f) {
                        return f.getName().endsWith(".txt") || f.isDirectory();
                    }

                    @Override
                    public String getDescription() {

                        return "Select .txt file to encrypt/decrypt";

                    }
                });
                jfc.showOpenDialog(null);
                JTFFilePath.setText(jfc.getSelectedFile().getPath());
                textlnTAFile("File selected: " + jfc.getSelectedFile().getPath());
            }
        });
        /**
         * Select folder to save the output file
         */
        JBOutputFD.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser jfc = new JFileChooser();
                jfc.setMultiSelectionEnabled(false);
                jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                jfc.setCurrentDirectory(new File(Master.aDir));
                jfc.showOpenDialog(null);
                JTFOutput.setText(jfc.getSelectedFile().getPath());
                textlnTAFile("Output folder selected: " + jfc.getSelectedFile().getPath());
            }
        });
        /**
         * Encrypts the selected text file into the selected output folder
         * Output file prefix: e
         */
        JBEncrypt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    File file = new File(JTFFilePath.getText());
                    File folder = new File(JTFOutput.getText());
                    if (!file.exists() || !file.getPath().endsWith(".txt")) {
                        throw new NullPointerException("Selected file: " + file.getPath() + " doesn't exist\n" +
                                "Select a valid text file");
                    }
                    if (!folder.exists() || !folder.isDirectory()) {
                        throw new NullPointerException("Selected output folder: " + folder.getPath() + " doesn't exist\n" +
                                "Select a valid folder");
                    }
                    String cf = Master.encryptFile(file.getPath(), folder.getPath());
                    textlnTAFile("File encrypted with RSA external public key\n" +
                            "Encrypted file: " + cf);
                }
                catch (Exception ex) {
                    textlnTAFile(ex.getMessage());
                }
            }
        });
        /**
         * Decrypts the selected text file into the selected output folder
         * Output file prefix: d
         */
        JBDecrypt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    File file = new File(JTFFilePath.getText());
                    File folder = new File(JTFOutput.getText());
                    if (!file.exists() || !file.getPath().endsWith(".txt")) {
                        throw new NullPointerException("Selected file: " + file.getPath() + " doesn't exist\n" +
                                "Select a valid text file");
                    }
                    if (!folder.exists() || !folder.isDirectory()) {
                        throw new NullPointerException("Selected output folder: " + folder.getPath() + " doesn't exist\n" +
                                "Select a valid folder");
                    }
                    String cf = Master.decryptFile(file.getPath(), folder.getPath());
                    textlnTAFile("File decrypted with RSA private key\n" +
                            "Decrypted file: " + cf);
                }
                catch(Exception ex) {
                    textlnTAFile(ex.getMessage());
                }
            }
        });
        /**
         * Exports the public key into the selected output folder
         * Format of key: (<n>,<e>)
         * Format of key: (<n>,<d>)
         */
        JBExportKey.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    File folder = new File(JTFOutput.getText());
                    if (!folder.exists() || !folder.isDirectory()) {
                        throw new Exception("Selected output folder: " + folder.getPath() + " doesn't exist\n" +
                                "Select a valid folder");
                    }
                    String[] kp = Master.exportKey(JCBSelKey.getSelectedIndex(), folder.getPath());
                    textlnTAFile("Current " + kp[0] + " key exported\n" +
                            "Exported " + kp[0] + " key: " + kp[1]);
                }
                catch(Exception ex) {
                    textlnTAFile(ex.getMessage());
                }
            }
        });
        /**
         * Import an external public key
         * Format of key: (<n>,<e>)
         */
        JBImpKey.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    File file = new File(JTFFilePath.getText());
                    if (!file.exists() || !file.getPath().endsWith(".txt")) {
                        throw new NullPointerException("Selected file: " + file.getPath() + " doesn't exist\n" +
                                "Select a valid text file");
                    }
                    String keyName = Master.importKey(JCBSelKey.getSelectedIndex(), file.getPath());
                    textlnTAFile("New " + keyName + " key imported\n" +
                            "Imported " + keyName + " key: " + file.getPath());
                }
                catch (Exception ex) {
                    textlnTAFile(ex.getMessage());
                }
            }
        });
         /**
         * generates private and public keys with RSA method
         */
        JBGenKey.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    Master.generateKey();
                    textlnTAFile("Keys generated");
                }
                catch (Exception ex) {
                    textlnTAFile(ex.getMessage());
                }
            }
        });
        /**
         * Use public key as external public key
         */
        JBPubKeyAsEx.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    Master.useAsExternalKey();
                    textlnTAFile("Public key used as external public key");
                }
                catch (Exception ex) {
                    textlnTAFile(ex.getMessage());
                }
            }
        });
    }

    /**
     * Prints a text into the JTAFile
     * @param text text to print
     */
    public void textlnTAFile(String text){
        textlnTA(JTAFile, text);
    }

    /**
     * Prints a text into the JTAKey
     * @param text text to print
     */
    public void textlnTAKey(String text){
        JTAKey.setText("");
        textlnTA(JTAKey, text);
    }

    /**
     * Prints the keys into the JTAKey
     * @param privateKey format of key: (<n>,<d>)
     * @param publicKey format of key: (<n>,<e>)
     * @param exPublicKey format of key: (<n>,<e>)
     */
    public void printKeys(String privateKey, String publicKey, String exPublicKey){
        textlnTAKey("Private key: " + privateKey + "\n" +
                "Public key: " + publicKey + "\n" +
                "External public key: " + exPublicKey);
    }

    public String[] getPrintedKeys(){
        String[] keys = JTAKey.getText().split("\n");
        keys[0] = keys[0].substring(13, keys[0].length());
        keys[1] = keys[1].substring(12, keys[1].length());
        keys[2] = keys[2].substring(21, keys[2].length());
        return keys;
    }

    /**
     * Appends a text to a J text area
     * @param ta J text area to fill
     * @param text text to print
     */
    private void textlnTA(JTextArea ta, String text) {
        ta.setText(ta.getText() + text + "\n");
    }

    /**
     * Sets the default output folder
     * @param folder default output folder
     */
    public void setStartFolder(String folder) {
        JTFOutput.setText(folder);
    }
}


