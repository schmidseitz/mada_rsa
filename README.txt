MadaRSA 

Developed by Claudio Seitz and Steven Schmid, 23.03.2016

The external public key is used to encrypt a text file. It's yourself's or anyone other's public key.
	Prefix encrypted file: e
The private key is used to decyrpt an encrypted text file. It's a secret key.
	Prefix decrypted file: d
The public key is generated as well as the private key and they are in relation.

Import or Export of keys:
	To or from a text file.
	Format public keys: (<n>,<e>)
	Format private key: (<n>,<d>)
It's dangerous to export the private key, because everyone, who knows the private key, can decrypt an encrypted text file, which was encrypted by the related public key.

Using: Encrypt a text file by the public of the receiver, then send it to the receiver. After this, the receiver decrypts the encrypted text by his own private key.

It's forbidden to modify this software.
